port module Main exposing (..)

import Browser
import Browser.Events
import Date
import Dict
import Functions
import Html exposing (Html)
import Html.Attributes as Attr
import Html.Events as Events
import Http
import Json.Decode as Decode
import Json.Decode.Pipeline
import Json.Encode as Encode
import List.Extra
import Task
import Time


port log : String -> Cmd msg


port login : () -> Cmd msg


port signup : () -> Cmd msg


port updateChart : Data -> Cmd msg


port gotUserdata : (Decode.Value -> msg) -> Sub msg


port setUserdata : Encode.Value -> Cmd msg


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { data = Nothing
      , userdata = Nothing
      , weightform = Nothing
      , aftersignupform = Nothing
      , loginform = Nothing
      , flags = flags
      , time = Time.millisToPosix 0
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ gotUserdata (Decode.decodeValue userDecoder >> GotUserdata)
        , Time.every 1000 Tick
        , Browser.Events.onKeyDown
            (Decode.field "key" Decode.string
                |> Decode.andThen
                    (\key ->
                        case String.toUpper key of
                            "O" ->
                                Decode.succeed OpenWeightform

                            _ ->
                                Decode.fail "Other Key"
                    )
            )
        ]


type alias Userdata =
    { full_name : String
    , weights : List Weightpoint
    , birthdate : Maybe Date.Date
    , sex : Maybe Sex
    , id : String
    , token : String
    }


type alias Weightpoint =
    { weight : Float
    , height : Int
    , time : Time.Posix
    }


type alias Graphpoint =
    { year : Int
    , week : Int
    , points : Float
    }


type Sex
    = Female
    | Male


userDecoder : Decode.Decoder (Maybe Userdata)
userDecoder =
    Decode.oneOf
        [ Decode.map Just <|
            (Decode.succeed Userdata
                |> Json.Decode.Pipeline.required "full_name" Decode.string
                |> Json.Decode.Pipeline.optional "weights" (Decode.list weightpointDecoder) []
                |> Json.Decode.Pipeline.optional "birthdate" (Decode.int |> Decode.map Date.fromRataDie |> Decode.maybe) Nothing
                |> Json.Decode.Pipeline.optional "sex" (Decode.maybe sexDecoder) Nothing
                |> Json.Decode.Pipeline.required "_id" Decode.string
                |> Json.Decode.Pipeline.required "_token" Decode.string
            )
        , Decode.null Nothing
        ]


weightpointDecoder : Decode.Decoder Weightpoint
weightpointDecoder =
    Decode.succeed Weightpoint
        |> Json.Decode.Pipeline.required "weight" Decode.float
        |> Json.Decode.Pipeline.required "height" Decode.int
        |> Json.Decode.Pipeline.required "time" (Decode.int |> Decode.map Time.millisToPosix)


sexDecoder : Decode.Decoder Sex
sexDecoder =
    Decode.andThen
        (\s ->
            case s of
                "female" ->
                    Decode.succeed Female

                "male" ->
                    Decode.succeed Male

                _ ->
                    Decode.fail ""
        )
        Decode.string


userEncoder : Userdata -> Encode.Value
userEncoder user =
    Encode.object <|
        List.filterMap identity
            [ Just ( "full_name", Encode.string user.full_name )
            , Just ( "weights", Encode.list weightpointEncoder user.weights )
            , Maybe.map (\date -> ( "birthdate", Encode.int <| Date.toRataDie date )) user.birthdate
            , Maybe.map (\sex -> ( "sex", sexEncoder sex )) user.sex
            , Just ( "_id", Encode.string user.id )
            ]


weightpointEncoder : Weightpoint -> Encode.Value
weightpointEncoder point =
    Encode.object
        [ ( "weight", Encode.float point.weight )
        , ( "height", Encode.int point.height )
        , ( "time", Encode.int <| Time.posixToMillis point.time )
        ]


sexEncoder : Sex -> Encode.Value
sexEncoder sex =
    Encode.string
        (case sex of
            Female ->
                "female"

            Male ->
                "male"
        )


type alias Data =
    List Dataset


type alias Dataset =
    { id : String
    , name : String
    , color : String
    , percentages : List Graphpoint
    }


graphpointEncoder : Graphpoint -> Encode.Value
graphpointEncoder gp =
    Encode.object
        [ ( "year", Encode.int gp.year )
        , ( "week", Encode.int gp.week )
        , ( "points", Encode.float gp.points )
        ]


graphpointDecoder : Decode.Decoder Graphpoint
graphpointDecoder =
    Decode.succeed Graphpoint
        |> Json.Decode.Pipeline.required "year" Decode.int
        |> Json.Decode.Pipeline.required "week" Decode.int
        |> Json.Decode.Pipeline.required "points" Decode.float



-- dataEncoder : Data -> Encode.Value
-- dataEncoder =
--     Encode.list datasetEncoder


datasetEncoder : Dataset -> Encode.Value
datasetEncoder set =
    Encode.object
        [ ( "_id", Encode.string set.id )
        , ( "name", Encode.string set.name )
        , ( "color", Encode.string set.color )
        , ( "percentages", Encode.list graphpointEncoder set.percentages )
        ]


dataDecoder : Decode.Decoder Data
dataDecoder =
    Decode.list
        (Decode.succeed Dataset
            |> Json.Decode.Pipeline.required "_id" Decode.string
            |> Json.Decode.Pipeline.required "name" Decode.string
            |> Json.Decode.Pipeline.optional "color" Decode.string ""
            |> Json.Decode.Pipeline.required "percentages"
                (Decode.oneOf
                    [ Decode.list graphpointDecoder

                    -- included for parsing old version of percentages
                    , Decode.map
                        (List.indexedMap
                            (\i p ->
                                let
                                    ( year, week ) =
                                        Functions.oldWeekToWeekDate i
                                in
                                Graphpoint year week p
                            )
                        )
                        (Decode.list Decode.float)
                    ]
                )
        )


type alias Flags =
    { -- currentweek : Int
    }


type alias Model =
    { data : Maybe Data
    , userdata : Maybe Userdata
    , weightform : Maybe { weight : String, height : String }
    , aftersignupform : Maybe { birthdate : String, sex : Maybe Sex }
    , loginform : Maybe { mail : String, pass : String }
    , flags : Flags
    , time : Time.Posix
    }


type Msg
    = Login
    | Signup
    | GotData (Result Http.Error Data)
    | GotUserdata (Result Decode.Error (Maybe Userdata))
    | Submit
    | SubmitAt Time.Posix
    | Submitted (Result Http.Error ())
    | OpenWeightform
    | CloseWeightform
    | InputWeight String
    | InputHeight String
    | InputBirthdate String
    | InputSex Sex
    | SubmitAfterSignup
    | Tick Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Login ->
            ( model, login () )

        Signup ->
            ( model, signup () )

        GotData (Ok data) ->
            ( { model | data = Just data }
            , updateChart data
            )

        GotData (Err _) ->
            ( model, log "todo: GotData Err" )

        GotUserdata (Ok Nothing) ->
            ( { model | userdata = Nothing, weightform = Nothing, aftersignupform = Nothing, data = Nothing }, Cmd.none )

        GotUserdata (Ok (Just user)) ->
            ( case ( user.birthdate, user.sex ) of
                ( Just _, Just _ ) ->
                    { model | userdata = Just user, weightform = Nothing, aftersignupform = Nothing, data = Nothing }

                ( date, sex ) ->
                    { model
                        | userdata = Just user
                        , weightform = Nothing
                        , aftersignupform =
                            Just
                                { birthdate = Maybe.withDefault "" (Maybe.map (Date.toIsoString >> String.left 10) date)
                                , sex = sex
                                }
                    }
            , Http.request
                { method = "POST"
                , headers = [] -- Http.header "Authorization" (String.append "Bearer " user.token) ]
                , url = "/api/submit"
                , body =
                    Http.jsonBody
                        (datasetEncoder
                            { name = user.full_name
                            , percentages = calcPercentages user
                            , id = user.id
                            , color = ""
                            }
                        )
                , expect = Http.expectWhatever Submitted
                , timeout = Nothing
                , tracker = Nothing
                }
            )

        GotUserdata (Err _) ->
            ( model, log "whoops" )

        InputWeight s ->
            ( { model | weightform = Maybe.map (\f -> { f | weight = s }) model.weightform }, Cmd.none )

        InputHeight s ->
            ( { model | weightform = Maybe.map (\f -> { f | height = s }) model.weightform }, Cmd.none )

        Submit ->
            ( model, Task.perform SubmitAt Time.now )

        SubmitAt time ->
            case ( model.userdata, model.weightform ) of
                ( Just user, Just form ) ->
                    case ( String.toFloat form.weight, String.toInt form.height ) of
                        ( Just weight, Just height ) ->
                            ( { model | weightform = Nothing }
                            , setUserdata <|
                                userEncoder
                                    { user
                                        | weights =
                                            user.weights
                                                ++ [ { weight = weight
                                                     , height = height
                                                     , time = time
                                                     }
                                                   ]
                                    }
                            )

                        _ ->
                            ( model, log "couldn't parse weight/height" )

                _ ->
                    ( model, log "cannot set weight when not logged in" )

        Submitted _ ->
            let
                token =
                    Maybe.withDefault "" (Maybe.map .token model.userdata)
            in
            ( model
            , Http.request
                { method = "GET"
                , headers = [] -- Http.header "Authorization" (String.append "Bearer " token) ]
                , url = "/api/data"
                , body = Http.emptyBody
                , expect = Http.expectJson GotData dataDecoder
                , timeout = Nothing
                , tracker = Nothing
                }
            )

        OpenWeightform ->
            case model.userdata of
                Nothing ->
                    ( model, Cmd.none )

                Just user ->
                    let
                        last =
                            Maybe.withDefault { weight = 0, height = 0, time = Time.millisToPosix 0 }
                                (lastWeightpoint user)
                    in
                    -- todo: defaults
                    ( { model | weightform = Just { weight = String.fromFloat last.weight, height = String.fromInt last.height } }, Cmd.none )

        CloseWeightform ->
            ( { model | weightform = Nothing }, Cmd.none )

        Tick time ->
            ( { model | time = time }
            , case model.data of
                Just _ ->
                    Cmd.none

                Nothing ->
                    -- todo: random loading charts!!
                    Cmd.none
              -- let
              --     ( lists, _ ) =
              --         Random.step (Random.list 3 <| Random.list 20 <| Random.float 0 100) <|
              --             Random.initialSeed (Time.posixToMillis time)
              -- in
              -- updateChart
              --     (List.indexedMap
              --         (\i list -> { id = String.fromInt i, name = "", percentages = list })
              --         lists
              --     )
            )

        InputBirthdate datestr ->
            ( { model | aftersignupform = Maybe.map (\f -> { f | birthdate = datestr }) model.aftersignupform }, Cmd.none )

        InputSex sex ->
            ( { model | aftersignupform = Maybe.map (\f -> { f | sex = Just sex }) model.aftersignupform }, Cmd.none )

        SubmitAfterSignup ->
            ( model
            , case ( model.userdata, model.aftersignupform ) of
                ( Just user, Just { birthdate, sex } ) ->
                    setUserdata <|
                        userEncoder { user | birthdate = Date.fromIsoString birthdate |> Result.toMaybe, sex = sex }

                _ ->
                    Cmd.none
            )


{-| Calculate the graph for a given user.

This is where the magic happens, where
raw data (weight, height, sex, age) is converted to "percentages"

-}
calcPercentages : Userdata -> List Graphpoint
calcPercentages user =
    user.weights
        -- should be sorted, but sort just in case
        |> List.sortBy (Time.posixToMillis << .time)
        -- calculate weekdate. initialise points to 100
        |> List.map
            (\wp ->
                { weight = wp.weight
                , height = wp.height
                , time = wp.time
                , weekDate = Functions.weekDate wp.time
                , points = 100
                }
            )
        -- keep only the last instance in case of duplicates
        |> List.reverse
        |> List.Extra.groupWhile (\a b -> a.weekDate == b.weekDate)
        |> List.map Tuple.first
        |> List.reverse
        -- calculate the points using a scan
        |> List.Extra.scanl1
            (\wp2 wp1 ->
                let
                    weight_diff =
                        wp2.weight - wp1.weight

                    bmr =
                        basalMetabolicRate
                            (Maybe.withDefault Male user.sex)
                            wp1.weight
                            (toFloat wp1.height)
                            (yearsBetween (Maybe.withDefault (Date.fromRataDie 730120) user.birthdate) (Date.fromPosix Time.utc wp2.time))
                in
                { wp2 | points = wp1.points + 7000 * weight_diff / bmr }
            )
        -- convert to Graphpoints (rounded)
        |> List.map
            (\{ weekDate, points } ->
                { year = Tuple.first weekDate
                , week = Tuple.second weekDate
                , points = (points * 10 |> round |> toFloat) / 10
                }
            )



-- Approximate number of years between dates (used for age calculation)


yearsBetween : Date.Date -> Date.Date -> Float
yearsBetween t1 t2 =
    toFloat (Date.toRataDie t1 - Date.toRataDie t2) / 365.242199



-- The BMR for a person with these "properties" (used for percentage calculation)


basalMetabolicRate : Sex -> Float -> Float -> Float -> Float
basalMetabolicRate sex mass_kg height_cm age_yr =
    let
        s =
            case sex of
                Female ->
                    -161

                Male ->
                    5
    in
    10.0 * mass_kg + 6.25 * height_cm - 5.0 * age_yr + s



-- The last weight for a user (prefilled into the weight form)


lastWeightpoint : Userdata -> Maybe Weightpoint
lastWeightpoint user =
    List.sortBy (\w -> Time.posixToMillis w.time) user.weights
        |> List.reverse
        |> List.head


hasSubmittedThisWeek : ( Int, Int ) -> Userdata -> Bool
hasSubmittedThisWeek weekdate user =
    List.any
        (\w -> Functions.weekDate w.time == weekdate)
        user.weights


quotes : List String
quotes =
    [ "Je bent mooi zoals je bent!"
    , "Kleine stappen maken een groot verschil!"
    , "Samen gaan we onze doelen bereiken!"
    , "Succes komt met met consistentie, niet met perfectie."
    , "Wees trots op jezelf!"
    ]



-- These weeks no data will be collected.
-- Everyone stays at the same level.


cheatWeeks : Dict.Dict ( number, number ) String
cheatWeeks =
    Dict.fromList
        [ ( ( 2023, 14 ), "Dit is een cheatweek, elke lijn blijft horizontaal. Kom komende maandag terug!" )
        ]


view : Model -> Html Msg
view model =
    let
        vandaagIsMaandag =
            Time.toWeekday Time.utc model.time == Time.Mon
    in
    case model.userdata of
        Nothing ->
            Html.div []
                [ Html.p [] [ Html.text "Maak een account aan, of log in" ]
                , Html.button [ Events.onClick Signup ] [ Html.text "Een nieuw account aanmaken" ]
                , Html.button [ Events.onClick Login ] [ Html.text "Inloggen" ]
                ]

        Just user ->
            Html.div []
                (List.filterMap identity
                    [ Just <|
                        Html.div []
                            [ Html.text (" Ingelogd als " ++ user.full_name ++ " ")
                            , Html.button [ Events.onClick Login ]
                                [ Html.text "Uitloggen"
                                ]
                            ]
                    , Just <|
                        case Dict.get (Functions.weekDate model.time) cheatWeeks of
                            Just message ->
                                Html.text message

                            Nothing ->
                                if hasSubmittedThisWeek (model.time |> Functions.weekDate) user then
                                    Html.text <|
                                        if vandaagIsMaandag then
                                            "Je hebt je gewicht van vandaag ingevuld. Tot volgende week."

                                        else
                                            "Je hebt je gewicht van afgelopen maandag ingevuld. Kom volgende week weer terug!"

                                else
                                    Html.button
                                        [ Events.onClick OpenWeightform, Attr.class "eye-catching-button" ]
                                        [ Html.text <|
                                            if vandaagIsMaandag then
                                                "Vul je gewicht van vandaag in!"

                                            else
                                                "Vul je gewicht van afgelopen maandag in!"
                                        ]
                    , Maybe.map viewWeightform model.weightform
                    , Maybe.map viewAfterSignupform model.aftersignupform
                    ]
                )


viewAfterSignupform : { sex : Maybe Sex, birthdate : String } -> Html Msg
viewAfterSignupform { sex, birthdate } =
    Html.form [ Attr.id "signupform", Attr.class "modal", Attr.action "#", Events.onSubmit SubmitAfterSignup ]
        [ Html.h1 [] [ Html.text "Extra gegevens" ]
        , Html.p []
            [ Html.text "Voor de berekening van jouw percentage houden we rekening met je leeftijd en geslacht. Deze gegevens worden (net als je gewicht) niet met anderen gedeeld." ]
        , Html.label []
            [ Html.text "Geboortedatum"
            , Html.input
                [ Attr.id "birthdate"
                , Attr.required True
                , Attr.type_ "date"
                , Attr.value birthdate
                , Events.onInput InputBirthdate
                ]
                []
            ]
        , Html.label []
            [ Html.text "Man"
            , Html.input
                [ Attr.name "geslacht"
                , Attr.required True
                , Attr.type_ "radio"
                , Attr.checked (sex == Just Male)
                , Events.onInput (always (InputSex Male))
                ]
                []
            ]
        , Html.label []
            [ Html.text "Vrouw"
            , Html.input
                [ Attr.name "geslacht"
                , Attr.required True
                , Attr.type_ "radio"
                , Attr.checked (sex == Just Female)
                , Events.onInput (always (InputSex Female))
                ]
                []
            ]
        , Html.button [] [ Html.text "Ga verder!" ]
        ]


viewWeightform : { weight : String, height : String } -> Html Msg
viewWeightform { weight, height } =
    Html.form [ Attr.id "weightform", Attr.class "modal", Attr.action "#", Events.onSubmit Submit ]
        [ Html.div [ Events.onClick CloseWeightform, Attr.class "close" ] []
        , Html.h1 [] [ Html.text "Nieuwe weging" ]
        , Html.label []
            [ Html.text "Gewicht (kg met één decimaal)"
            , Html.input
                [ Attr.id "weight"
                , Attr.required True
                , Attr.type_ "number"
                , Attr.step "0.1"
                , Attr.min "0"
                , Attr.max "1000"
                , Attr.value weight
                , Events.onInput InputWeight
                ]
                []
            ]
        , Html.label []
            [ Html.text "Lengte (cm)"
            , Html.input
                [ Attr.id "height"
                , Attr.required True
                , Attr.type_ "number"
                , Attr.step "1"
                , Attr.min "20"
                , Attr.max "299"
                , Attr.value height
                , Events.onInput InputHeight
                ]
                []
            ]
        , Html.button [] [ Html.text "Invoeren!" ]
        ]
