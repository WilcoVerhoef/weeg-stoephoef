type Elm = Record<string, Module>

interface Module {
  init<F extends Flags | undefined = undefined, P extends Ports = AnyPorts>
    (options: F extends undefined ? { node: Node } : { node: Node, flags: F })
    : App<P>
}

type AnyPorts = Record<string, CmdPort<any> & SubPort<any>>
type Ports = Record<string, CmdPort<any> | SubPort<any>>
type Flags = null | boolean | number | string | FlagsArray | FlagsRecord;
interface FlagsArray extends Array<Flags> { }
interface FlagsRecord extends Record<string, Flags> { }

interface App<P extends Ports> {
  ports: P
}

interface CmdPort<V extends Flags> {
  subscribe(handler: (value: V) => void): void
  unsubscribe(handler: (value: V) => void): void
}

declare interface SubPort<V extends Flags> {
  send(value: V): void
}

export const Elm: Elm
