import * as Identity from "netlify-identity-widget"
import { Elm, SubPort, CmdPort } from "./Main.elm"
import Chart from 'chart.js/auto'

declare global {
  var netlifyIdentity: typeof Identity
}

const node = document.getElementById("app")
if (node == null) throw 'Could not find the #app element'

type Userdata = {
  last_weight: number,
  _id: string
  _token: string
}

type Flags = {
  currentweek: number,
};
type Ports = {
  log?: CmdPort<string>,
  login: CmdPort<null>,
  signup: CmdPort<null>,
  updateChart: CmdPort<Data>,
  setUserdata: CmdPort<Userdata>,
  gotUserdata: SubPort<Userdata | null>
}

const first_week = new Date("2023-01-02")
const now = new Date()
const this_week = new Date(now.getTime() - ((now.getDay() + 6) % 7) * 86400000)
const currentweek = Math.round((this_week.getTime() - first_week.getTime()) / 604800000)
const flags: Flags = { currentweek }

const app = Elm.Main.init<Flags, Ports>({ node, flags })
app.ports.log?.subscribe(console.log)

// I think this is the only thing we need to change every year
// (maybe make it automatic some time?)
const year = 2025;

// IDENTITY

app.ports.login.subscribe(() => { netlifyIdentity.open('login'); });
app.ports.signup.subscribe(() => { netlifyIdentity.open('signup'); });

function gotUser(user: Identity.User | null = null) {
  console.log(`user:`, user)
  if (user == null) app.ports.gotUserdata.send(null);
  else app.ports.gotUserdata.send({ ...user.user_metadata, _id: user.id, _token: user.token?.access_token } as Userdata);

  let kiek = document.getElementById("vreetkiekjes");
  if (kiek) kiek.hidden = user == null;
}

netlifyIdentity.on('init', async u => {
  // @ts-ignore "update" is unknown
  u = await u?.update({});
  gotUser(u)
  // @ts-ignore "store is unknown"
  setTimeout(() => netlifyIdentity.store.modal.logo = false)
})
netlifyIdentity.on('login', gotUser)
netlifyIdentity.on('logout', gotUser)

app.ports.setUserdata.subscribe(async (data) => {
  // @ts-ignore "update" is unknown
  const user = await netlifyIdentity.currentUser()?.update({ data })
  gotUser(user)
})

// netlifyIdentity.init({ logo: false, locale: "nl" })
// gotUser(netlifyIdentity.currentUser())

// CHART

let chart = new Chart(
  document.getElementById("chart") as any,
  {
    type: "line",
    data: { datasets: [] },
    options: {
      scales: { y: { suggestedMax: 100 } },
      plugins: {
        legend: { display: false },
        title: {
          display: true,
          text: year.toString()
        }
      },
      spanGaps: true
    }
  }
)

type Data = {
  id: string
  name: string
  percentages: number[]
}[]

app.ports.updateChart.subscribe((data: Data) => {
  // const longest = Math.max(...data.map(set => set.percentages.length))

  // update labels
  chart.data.labels ??= [];
  chart.data.labels.length = 0;
  for (let i = 0; i < 53; i++) {
    chart.data.labels[i] = `Week ${i + 1}`;
  }

  // Remove datasets when some are removed
  a: for (const set of chart.data.datasets) {
    for (const newset of data) {
      if (newset.id == (set as any).id) {
        continue a;
      }
    }
    chart.data.datasets.length = 0;
    break;
  }

  // upsert datasets
  for (const set of data as [any]) {
    const og_dataset = chart.data.datasets.find((x: any) => x.id === set.id)
    set.label = set.name;
    set.data = [];
    for (const point of set.percentages) {
      if (point.year == year) set.data[point.week - 1] = point.points
    }
    const first = set.data.find(x => x !== undefined);
    set.data = set.data.map(x => x - first + 100);
    if (!og_dataset) chart.data.datasets.push(set)
    else Object.assign(og_dataset, set)
  }
  chart.update();

  updateVreetkiekjes();
});

function updateVreetkiekjes() {
  vreetkiekjes.innerHTML = '';
  for (const set of chart.data.datasets) {
    const img = document.createElement("img");
    img.src = `./${set.name.toLowerCase().trim()}.png`;
    img.style.borderColor = set.borderColor;
    img.tabIndex = -1;
    const li = document.createElement("li");
    li.appendChild(img);
    vreetkiekjes.appendChild(li)
  }
}


// AI Quote

// const quote_p = document.getElementById("quote") as HTMLElement;
// const new_quote_button = document.getElementById("new_quote") as HTMLElement;
// new_quote_button.onclick = new_quote;
// new_quote();

// async function new_quote(){
//   quote_p.innerText = "";
//   new_quote_button.hidden = true;
//   const ev = new EventSource("/api/quote");
//   ev.onmessage = (e) => {
//     if (e.data == "[DONE]") {
//       new_quote_button.hidden = false;
//       return ev.close();
//     }
//     let token = JSON.parse(e.data).choices[0].text;
//     quote_p.innerText += token;
//     console.log(token);
//   };
//   ev.onerror = () => { new_quote_button.hidden = false };
// };

// OTHER


// @ts-ignore comment
window.app = app; window.chart = chart; window.Chart = Chart;
