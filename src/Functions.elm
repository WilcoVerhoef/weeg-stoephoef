module Functions exposing (..)

import Date
import Time


weekDate : Time.Posix -> ( Int, Int )
weekDate time =
    let
        date =
            Date.fromPosix Time.utc time
    in
    ( Date.weekYear date, Date.weekNumber date )


oldWeekToWeekDate : Int -> ( Int, Int )
oldWeekToWeekDate w =
    let
        date =
            Date.fromWeekDate 2023 1 Time.Mon
                |> Date.add Date.Weeks w
    in
    ( Date.weekYear date, Date.weekNumber date )
