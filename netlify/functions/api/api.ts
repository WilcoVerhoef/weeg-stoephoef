import type { Handler, HandlerEvent, HandlerContext } from "@netlify/functions";
import fetch from 'node-fetch';


try {
  require('dotenv').config({ export: true });
} catch {
  console.warn("could not read .env");
}

const MONGODB_DATA_API_KEY = process.env.MONGODB_DATA_API_KEY;
const MONGODB_APP_ID = process.env.MONGODB_APP_ID;

if (MONGODB_DATA_API_KEY === undefined) throw "MONGODB_DATA_API_KEY not set";
if (MONGODB_APP_ID === undefined) throw "MONGODB_APP_ID not set";

const MONGODB_BASE_URI =
  `https://data.mongodb-api.com/app/${MONGODB_APP_ID}/endpoint/data/v1/action`;

const headers = {
  "Content-Type": "application/json",
  "Access-Control-Request-Headers": "*",
  "api-key": MONGODB_DATA_API_KEY,
}

const query = {
  collection: "people",
  database: "weeg-stoephoef-db",
  dataSource: "weeg-stoephoef",
};

interface Person {
  _id: string;
  name: string;
  percentages: number[];
}

const handler: Handler = async (event: HandlerEvent, context: HandlerContext) => {
  // console.log("event", event);
  // console.log("context", context)

  console.log(`edge function, invoked by ${JSON.stringify(context.clientContext?.user?.user_metadata.full_name)}`);
  console.log(event.path);

  console.log("clientContext", context.clientContext)

  // if (!context.clientContext?.user) return { statusCode: 401, body: "Unauthorized" }

  // context.clientContext?.identity?.url;
  // context.clientContext?.identity?.token;

  switch (event.path) {
    case "/api/data": {
      console.log("handling /api/data")
      const options = {
        method: "POST",
        headers,
        body: JSON.stringify({
          ...query,
          filter: { enabled: true }
        })
      };
      const res = await fetch(`${MONGODB_BASE_URI}/find`, options);
      const data = await res.json() as { documents: [Person] };
      for (const p of data.documents) {
        // not the most safe way to hide the _id but oh well.
        // will not be necessary once switched to non-edge func.
        p._id = p._id.slice(16);
      }
      return {
        statusCode: 200,
        body: JSON.stringify( data.documents )
      }
    }

    case "/api/submit": {
      console.log("handling /api/submit")
      if (!event.body) return { statusCode: 400 }
      const submission = JSON.parse(event.body);
      const _id = submission._id.slice(8).replaceAll('-',''); // ctx.account.id
      delete submission._id;
      delete submission.enabled;
      const options = {
        method: "POST",
        headers,
        body: JSON.stringify({
          ...query,
          filter: { _id },
          update: { $set: submission },
          upsert: true,
        })
      };
      const res = await fetch(`${MONGODB_BASE_URI}/updateOne`, options);
      const data = await res.json(); // as { documents: [Person] };
      return {
        statusCode: 200,
        body: JSON.stringify( data )
      }
    }
  }

  return { statusCode: 404 }
};

export { handler };
