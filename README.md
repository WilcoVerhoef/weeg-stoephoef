# Weeg Stoephoef

## Vereisten

- Installeer Yarn

## Bouwen

- `yarn serve` start de dev server (auto-rebuild bij elke wijziging in de broncode)
- `yarn build` bouw de site, zie het nieuwe `dist` mapje voor het resultaat.

Let erop dat accounts & database requests niet werken zonder de juiste keys!

## Gebruikte technologieën

Een sterretje \* geeft aan dat dit de eerste keer is dat ik deze technologie heb gebruikt :o

- \* Yarn (package manager)
- Elm + TypeScript + SCSS en een beetje HTML voor de front-end
- \* Parcel om bovenstaande te bundelen
- \* Netlify voor identity (login & user metadata zoals absolute gewichten)
- \* Netlify edge-functions als API endpoint
- Netlify voor automatische deploys (vanaf de main branch)
- \* Deno/TypeScript voor de API endpoint 
- \* MongoDB Atlas als database voor data die voor iedereen met account zichtbaar is (de berekende "percentages")
